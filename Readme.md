# Task

Дан файл цілих чисел.

Створити новий файл цілих чисел, що містить довжини всіх серій вихідного файлу (серією називається набір послідовно розташованих однакових елементів, а довжиною серії - кількість цих елементів). Наприклад, для вихідного файлу з елементами 1, 5, 5, 5, 4, 4, 5 вміст результуючого файлу повинна бути наступним: 1, 3, 2, 1. 

Результат вивести в файл з таким же ім'ям, як і вихідний, але з розширенням «.out».

Завдання повинні бути виконано у вигляді додатку WindowsForms. Всі імена файлів запитуються через стандартні діалоги відкриття і збереження файлу.

# Executable file

Вы можете собрать проект из исходников или [скачать готовую сборку](https://www.dropbox.com/s/vikutzpquyek8cv/laba4.zip?dl=0)