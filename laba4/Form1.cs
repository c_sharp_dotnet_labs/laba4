﻿using System.Windows.Forms;
using System.IO;

namespace JSON_Editor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            lbOutExplanation.Text +=
"\n(a sery is a set of consecutive identical elements, " +
"and the length of the series is the number of these elements).";
            openToolStripMenuItem.Click += OpenToolStripMenuItem_Click;
            saveToolStripMenuItem.Click += SaveToolStripMenuItem_Click;
            exitToolStripMenuItem.Click 
                += (object sender, System.EventArgs e) => Application.Exit();
            btnSave.Click += SaveToolStripMenuItem_Click;
        }

        private string outFileName;

        private void SaveToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(rtbOut.Text) 
                || string.IsNullOrEmpty(outFileName) )
            {
                MessageBox.Show(this,
"Nothing to save. Open input file from that will be produced output and then Save.",
                    "Nothing to save");
                return;
            }
            var folderBrowser = new FolderBrowserDialog();
            folderBrowser.UseDescriptionForTitle = true;
            folderBrowser.Description
                = "Choose directory where you want to save output file.";
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                string outFilePath = $"{folderBrowser.SelectedPath}/{outFileName}";
                using (StreamWriter strWrt = File.CreateText(outFilePath))
                {
                    strWrt.Write(rtbOut.Text);
                }
            }
        }

        private void OpenToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    lbInputFileName.Text = $"{ openFileDialog.SafeFileName } :";
                    outFileName = Path.GetFileNameWithoutExtension(filePath) + ".out";
                    lbOutFileName.Text = outFileName + " :";

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                    }

                    rtbInput.Text = fileContent;
                    rtbOut.Text = Program.ProcessData(fileContent);
                }
            }
        }
    }
}
