using System;
using System.Text;
using System.Windows.Forms;

namespace JSON_Editor
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            _mainWindow = new Form1();
            Application.Run(_mainWindow);
        }

        private static Form1 _mainWindow;

        private static int ConvertWithMyErrorMessage(string num_str)
        {
            int num = -1;
            try
            {
                num = Convert.ToInt32(num_str);
            }
            catch (System.FormatException ex)
            {
                MessageBox.Show(_mainWindow,
                    "Unable to work with file you have chosen."
            + $"\nPlease, choose another file or edit this file. \n{ ex.Message }",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return num;
        }

        internal static string ProcessData(string input)
        {
            string[] arr = input.Split(',');

            var sb = new StringBuilder();

            int seqNum = ConvertWithMyErrorMessage(arr[0]); // number from sequence
            int seqCount = 0; // count of numbers from sequence
            foreach (var num_str in arr)
            {
                int num = ConvertWithMyErrorMessage(num_str);
                if (num != seqNum)
                {
                    sb.Append(seqCount).Append(",\t");
                    seqCount = 1;
                    seqNum = num;
                } else
                {
                    ++seqCount;
                }
            }

            sb.Append(seqCount);

            return sb.ToString();
        }


        
    }
}
